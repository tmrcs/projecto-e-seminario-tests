const express = require('express');
const path = require('path');
const net = require('net');
const fs = require('fs');

const debug = new (require('../../debug_utils/DebugInfo2Console'))("Web Server");

var app = express();

app.use(express.static(path.join(__dirname, 'public')));

var views_dir = getBaseDirectoryOfProject() + '/WebUI/WebCLient/views/';

app.get('/', function(req, res){
    fs.readFile(views_dir + 'simple_button_view.html', function(err, data){
        if(err){
            debug(err);
        }
        res.end(data);
    });
});

app.get('/set', function(req, res){
    debug.print("SET REQUEST MADE");

    const client = net.connect({port: 3000}, function(){
        client.write('SET');
        client.end();
    });

    client.on('error', function(err){
        debug("ERROR OCCURRED: "+err);
    });

    client.on('end', function(){
        res.end();
    });
});

app.get('/reset', function(req, res){
    debug.print("## WEB SERVER - RESET REQUEST MADE");

    const client = net.connect({port: 3000}, function(){
        client.write('RESET');
        client.end();
    });

    client.on('error', function(err){
        debug.print("ERROR OCCURRED: "+err);
    });

    client.on('end', function(){
        res.end();
    });
});

function getBaseDirectoryOfProject(){
    var base_dir_name = "ValueFromWebUI";

    var curr_dir_path = __dirname;

    var base_dir_path = curr_dir_path.substring(
        0,
        curr_dir_path.indexOf(base_dir_name) + base_dir_name.length
    );

    return base_dir_path;
}

module.exports = app;
