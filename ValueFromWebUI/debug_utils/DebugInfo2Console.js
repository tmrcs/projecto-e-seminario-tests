function debugger_module(module_name){
    this.module_name = module_name;
}

debugger_module.prototype.print = function(message){
    console.log("## "+ this.module_name + " - " + message);
}

module.exports = debugger_module;