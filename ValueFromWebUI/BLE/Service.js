/**
 * This service will listen for connections on a socket,
 * and make changes to the value of the ExhaustionCharacteristic.
 * */

const bleno = require('bleno');
const PrimaryService = bleno.PrimaryService;
const characteristic = require('./ExhaustionCharacteristic');
const Characteristic = bleno.Characteristic;

const net = require('net');

const debug = new (require('../debug_utils/DebugInfo2Console'))("BLE");

/**
 * Setup of the BLE service
 * */

module.exports.startService = function() {
    //const ExhaustionCharacteristic = new characteristic();

    bleno.on('stateChange', function (state) {
        debug.print('on -> stateChange: ' + state);

        if (state === 'poweredOn') {
            bleno.startAdvertising('echo', ['fffffffffffffffffffffffffffffff0']);
        } else {
            bleno.stopAdvertising();
        }
    });

    bleno.on('advertisingStart', function (error) {
        debug.print('on -> advertisingStart: ' + (error ? 'error ' + error : 'success'));

        if (!error) {
            bleno.setServices([
                new PrimaryService({
                    uuid: 'fffffffffffffffffffffffffffffff0',
                    characteristics: [
                        // Define our characteristics
                        //ExhaustionCharacteristic
                        new Characteristic({
                            uuid: 'fffffffffffffffffffffffffffffff1', // or 'fff1' for 16-bit
                            properties: [], // can be a combination of 'read', 'write', 'writeWithoutResponse', 'notify', 'indicate'
                            secure: [], // enable security for properties, can be a combination of 'read', 'write', 'writeWithoutResponse', 'notify', 'indicate'
                            value: null, // optional static value, must be of type Buffer
                            descriptors: [],
                            onReadRequest: null,
                            onWriteRequest: null, // optional write request handler, function(data, offset, withoutResponse, callback) { ...}
                            onSubscribe: null,
                            onUnsubscribe: null,
                            onNotify: null, // optional notify sent handler, function() { ...}
                            onIndicate: null // optional indicate confirmation received handler, function() { ...}
                        })
                    ]
                })
            ]);

            // BLE Service will now be running, so we can start the server and listen for commands.
            //startListningOnSocket();
        }
    });

    function startListningOnSocket() {
        const mapper = {
            SET: function () {
                ExhaustionCharacteristic._value = new Buffer("User is Exhausted!");
                debug.print("New Exhaustion State: " + ExhaustionCharacteristic._value)

                if (ExhaustionCharacteristic._updateValueCallback)
                    ExhaustionCharacteristic._updateValueCallback(ExhaustionCharacteristic._value);
            },
            RESET: function () {
                ExhaustionCharacteristic._value = new Buffer("User is Fine!"); // can not use the word 'NOT'.
                debug.print("New Exhaustion State: " + ExhaustionCharacteristic._value)

                if (ExhaustionCharacteristic._updateValueCallback)
                    ExhaustionCharacteristic._updateValueCallback(ExhaustionCharacteristic._value);
            }
        }

        /**
         * Setup of the socket on which we will listen for a command (Set/Reset)
         * It is expected that the client connects, sends the command he wishes to execute, and then terminates the connection.
         * */

        const server = net.createServer(function (connection) {
            debug.print("Client Connected to Socket");

            /**
             * The value sent by the client should be one of 2 values: SET or RESET
             * */
            connection.on('data', function (data) {
                if (data == 'SET' || data == 'RESET') {
                    debug.print("Command received: "+data);
                    mapper[data]();
                }
            });

            connection.on('end', function () {
                debug.print("Client Disconnected from Socket");
            });
        });

        server.listen(3000, function () {
            debug.print("Socket is listening on port=3000");
        });
    }
}