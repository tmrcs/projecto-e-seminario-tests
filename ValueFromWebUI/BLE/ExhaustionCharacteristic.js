var util = require('util');

var bleno = require("bleno");

var BlenoCharacteristic = bleno.Characteristic;

var Exhaustion = function() {
    Exhaustion.super_.call(this, {
        uuid: 'ec0e',
        properties: ['read', 'notify'],
        value: null     // Must be of type Buffer.
    });

    this._value = new Buffer(0);
    this._updateValueCallback = null;
};

util.inherits(Exhaustion, BlenoCharacteristic);

Exhaustion.prototype.onReadRequest = function(offset, callback) {
    console.log('ExhaustionCharacteristic - onReadRequest: value = ' + this._value.toString('hex'));

    callback(this.RESULT_SUCCESS, this._value);
};

Exhaustion.prototype.onSubscribe = function(maxValueSize, updateValueCallback) {
    console.log('ExhaustionCharacteristic - onSubscribe');

    this._updateValueCallback = updateValueCallback;
};

Exhaustion.prototype.onUnsubscribe = function() {
    console.log('ExhaustionCharacteristic - onUnsubscribe');

    this._updateValueCallback = null;
};

module.exports = Exhaustion;
