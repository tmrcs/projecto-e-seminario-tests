var util = require('util');

var bleno = require("bleno");

const rl = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

var BlenoCharacteristic = bleno.Characteristic;

var Fadiga = function() {
    Fadiga.super_.call(this, {
        uuid: 'fff1',
        properties: ['read', 'notify'],
        value: null     // Must be of type Buffer.
    });

    this._value = new Buffer(20);
    this._updateValueCallback = null;
};

// extend BlenoCharecteristics (which inherits (extends) from EventEmitter.
util.inherits(Fadiga, BlenoCharacteristic);

Fadiga.prototype.onReadRequest = function(offset, callback) {
    console.log('EchoCharacteristic - onReadRequest: value = ' + this._value.toString('hex'));

    rl.question('value to send? ', function(ans){
        this._value = new Buffer(ans);
        callback(this.RESULT_SUCCESS, this._value);

    })
};

Fadiga.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {
    this._value = data;

    console.log('EchoCharacteristic - onWriteRequest: value = ' + this._value);

    if (this._updateValueCallback) {
        console.log('EchoCharacteristic - onWriteRequest: notifying');

        this._updateValueCallback(this._value);
    }

    callback(this.RESULT_SUCCESS);
};

Fadiga.prototype.onSubscribe = function(maxValueSize, updateValueCallback) {
    console.log('EchoCharacteristic - onSubscribe');

    this._updateValueCallback = updateValueCallback;
};

Fadiga.prototype.onUnsubscribe = function() {
    console.log('EchoCharacteristic - onUnsubscribe');

    this._updateValueCallback = null;
};

module.exports = Fadiga;
