const bleno = require("bleno");

const MyChar = require("./Characteristic");

const MyPrimaryService = bleno.PrimaryService;

bleno.on('stateChange', function(state) {
    console.log('on -> stateChange: ' + state);

    if (state === 'poweredOn') {
        // The first param defines the name of the device.
        bleno.startAdvertising('Test', ['FFF0']);
    } else {
        bleno.stopAdvertising();
    }
});

bleno.on('advertisingStart', function(error) {
    console.log('on -> advertisingStart: ' + (error ? 'error ' + error : 'success'));

    if (!error) {
        bleno.setServices([
            new MyPrimaryService({
                uuid: 'FFF0',
                characteristics: [
                    new MyChar()
                ]
            })
        ]);
    }
});
